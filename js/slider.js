let elIndex = 0;
let current;;
let slideLargeImg = document.getElementsByClassName('slide-img-large');
for(let i = 0; i < slideLargeImg.length; i++) {
  slideLargeImg[i].classList.add('notShow');
}
slideLargeImg[elIndex].classList.add('current');
let slideImg = document.getElementsByClassName('slide-img');
for(let j = 0; j < slideImg.length; j++) {
  slideImg[j].addEventListener('click', function(e){
    //current = document.querySelector('.current');
    //this.src = current.src;
  let elIndex = Array.from(slideImg).indexOf(e.target);
    for(let a = 0; a < slideImg.length; a++) {
      slideLargeImg[a].classList.remove('current');
    }
    slideLargeImg[elIndex].classList.add('current');
  })
}


function slider(){
  const slider = document.querySelector('.slider-container'),
  slides = Array.from(document.querySelectorAll('.slide'))
  
let isDragging = false,
  startPos = 0,
  currentTranslate = 0,
  prevTranslate = 0,
  animationID = 0,
  currentIndex = 0

slides.forEach((slide, index) => {
  const slideImage = slide.querySelector('img')
  slideImage.addEventListener('dragstart', (e) => e.preventDefault())
  
  // ივენთების დამათება თითით შეხების შემთხვევაში
  slide.addEventListener('touchstart', touchStart(index))
  slide.addEventListener('touchend', touchEnd)
  slide.addEventListener('touchmove', touchMove)

  // ივენთების დამათება მაუსის შემთხვევაში
  //slide.addEventListener('mousedown', touchStart(index))
  //slide.addEventListener('mouseup', touchEnd)
  //slide.addEventListener('mouseleave', touchEnd)
  //slide.addEventListener('mousemove', touchMove)
})

function touchStart(index) {
  return function (event) {
    currentIndex = index
    startPos = getPositionX(event)
    isDragging = true

    animationID = requestAnimationFrame(animation)
    slider.classList.add('grabbing')
  }
}

function touchEnd() {
  isDragging = false
  cancelAnimationFrame(animationID)
  const movedBy = currentTranslate - prevTranslate

  if (movedBy < -100 && currentIndex < slides.length - 1) currentIndex += 1

  if (movedBy > 100 && currentIndex > 0) currentIndex -= 1
  
  setPositionByIndex()
  setSliderButtonColor()
  slider.classList.remove('grabbing')
}

function touchMove(event) {
  if (isDragging) {
    const currentPosition = getPositionX(event)
    currentTranslate = prevTranslate + currentPosition - startPos
  }
}

function getPositionX(event) {
  return event.type.includes('mouse') ? event.pageX : event.touches[0].clientX
}

function animation() {
  setSliderPosition()
  if (isDragging) requestAnimationFrame(animation)
}

function setSliderPosition() {
  slider.style.transform = `translateX(${currentTranslate}px)`
}

function setPositionByIndex() {
  currentTranslate = currentIndex * -window.innerWidth
  prevTranslate = currentTranslate
  setSliderPosition()
}

//ფუნქცია რომ სლაიდერის გადასქროლვასთან ერთად შეიცვალოს მასზე ზედ არსებული წრეების ფერი
function setSliderButtonColor() {
  var prodImageSlides= document.getElementsByClassName("sliderButton")
  prodImageSlides[0].style.backgroundColor = '#D9EC00';

  for(let i = 0; i < prodImageSlides.length; i++){
    prodImageSlides[i].style.backgroundColor = '#C4C4C4';
  }
  prodImageSlides[currentIndex].style.backgroundColor = '#D9EC00';
  }
  setSliderButtonColor()

}

slider();







  



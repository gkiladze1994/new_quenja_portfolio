let changeSum = document.getElementById('changeSum');
let counter = changeSum.textContent;
let increment = document.getElementById('increment');
let decrement = document.getElementById('decrement');

increment.addEventListener('click', function() {
  counter++;
  changeSum.innerHTML = counter;
})

decrement.addEventListener('click', function() {
  if(counter!= 0) {
    counter--;
    changeSum.innerHTML = counter;
  }
})

let categoryWrapper = document.querySelectorAll('.categoryBtn');
for(let i = 0; i < categoryWrapper.length; i++){
  categoryWrapper[i].addEventListener('mouseenter', function() {
    this.style.backgroundColor = "#D9EC00";
    this.style.color = "#000000";
  })

  categoryWrapper[i].addEventListener('mouseleave', function() {
    this.style.backgroundColor = "transparent";
    this.style.color = "#ffffff";
  })
}
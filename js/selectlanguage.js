let select = document.querySelector('.select-language');
let dropdown = document.querySelector('.dropdown');
let dropdownContent = document.querySelector('.dropdown-content');
let selectedLanguage = document.getElementById('selected-language');
let link = select.getElementsByTagName('a');
dropdown.addEventListener('mouseenter',function() {
  dropdownContent.style.visibility = "visible";
  dropdownContent.style.opacity = 1;
})

dropdown.addEventListener('mouseleave',function() {
  dropdownContent.style.visibility = "hidden";
  dropdownContent.style.opacity = 0;
})
for(let i = 0; i < link.length; i++) {
  link[i].addEventListener('click', function() {
    let text = this.text;
    selectedLanguage.innerHTML = text;
    dropdownContent.style.visibility = "hidden";
    dropdownContent.style.opacity = 0;
  })
}
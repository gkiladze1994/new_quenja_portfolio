let closeBtn = document.querySelector('.closeBtn');
let mobileContent = document.querySelector('.mobile-nav-content');
let hiddenBtn = document.querySelector('.hiddenMenuBtn')
closeBtn.addEventListener('click', function() {
  mobileContent.classList.toggle('close');
})

hiddenBtn.addEventListener('click', function() {
  mobileContent.classList.remove('close');
})